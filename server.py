import socket
import sys

HOST = '' #symbolic name meaning all available interfaces
PORT = 5000 #Arbitrary non-privileges port
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

#Enlazar
try:
	s.bind((HOST, PORT))
except socket.error , msg:
	print 'Bind failed. Error Code : '+ str(msg[0]) + ' Message ' + msg[1]
	sys.exit()
print 'Socket bind complete'

s.listen(10)
print 'Socket now listening'
#now keep talking with the client
while 1:
	#wait to accept a connection - blocking call
	conn, addr = s.accept()
	#display client information
	print 'Connected with ' + addr[0] + ':' + str(addr[1])
	#now keep talking with the client
	data = conn.recv(1024)
	reply = 'OK...'+ data
	if not data:
		break

	conn.sendall(reply)

conn.close()
s.close()
