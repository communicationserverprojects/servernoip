import socket
import sys
from thread import *

HOST = '' #symbolic name meaning all available interfaces
PORT = 5000 #Arbitrary non-privileges port
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

#Enlazar
try:
	s.bind((HOST, PORT))
except socket.error , msg:
	print 'Bind failed. Error Code : '+ str(msg[0]) + ' Message ' + msg[1]
	sys.exit()
print 'Socket bind complete'

s.listen(10)
print 'Socket now listening'

#function for handling connections. This will be used to create threads
def clientthread(conn):
	#Sending message to connected client
	conn.send('Welcomme to the server. type somethinf and hit enter \n')
	#infinite loop so that function do not terminate and thread do not end.

	while True:
		
		#Receiving from client
		data = conn.recv(1024)
		reply = 'OK...' + data
		if not data:
			break
		conn.sendall(reply)

	#came out of loop
	conn.close()

#now keep talking with the client
while 1:
	#wait to accept a connection - blocking call
	conn, addr = s.accept()
	#display client information
	print 'Connected with ' + addr[0] + ':' + str(addr[1])
	#start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
	start_new_thread(clientthread ,(conn,))

s.close()
